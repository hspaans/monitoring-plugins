nagios-plugins-wordpress
========================

Nagios plugin for checking WordPress

Example configuration:

define hostgroup {
        hostgroup_name  wordpress-servers
        alias           WordPress servers
        members         server1.example.org,server3.example.org
        }

define service{
        use                             generic-service
        hostgroup_name                  wordpress-servers
        service_description             WordPress versions
        check_command                   check_nrpe_secure!check_wordpress_updates
        check_interval                  240
        notification_period             workhours
        }


nagios-plugins-spamassassin
===========================

Nagios plugin for checking Spamassassin

Example configuration:

define hostgroup {
        hostgroup_name  spamassassin-servers
        alias           SpamAssassin servers
        members         server1.example.org,server3.example.org
        }

define service{
        use                             generic-service
        hostgroup_name                  spamassassin-servers
        service_description             SpamAssassin versions
        check_command                   check_nrpe_secure!check_spamassassin_updates
        check_interval                  480
        notification_period             workhours
        }

define service{
        use                             generic-service
        hostgroup_name                  spamassassin-servers
        service_description             SpamAssassin Bayesian Database
        check_command                   check_nrpe_secure!check_spamassassin_bayesdb
        check_interval                  5
        }


nagios-plugins-clamav
=====================

Nagios plugin for checking ClamAV

Example configuration:

define hostgroup {
        hostgroup_name  clamav-servers
        alias           ClamAV servers
        members         server1.example.org,server3.example.org
        }

define service {
        use                             generic-service
        hostgroup_name                  clamav-servers
        service_description             Proc: clamd
        check_command                   check_nrpe!check_proc_clamd
        check_interval                  5
}

define service {
        use                             generic-service
        hostgroup_name                  clamav-servers
        service_description             Proc: freshclam
        check_command                   check_nrpe!check_proc_freshclam
        check_interval                  30
}

